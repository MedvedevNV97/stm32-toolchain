FROM ubuntu:22.04

WORKDIR /dev/

RUN apt update
#GCC for ARM
RUN apt -y install gcc-arm-none-eabi=15:10.3-2021.07-4
#GDB for ARM
RUN apt -y install gdb-multiarch=12.0.90-0ubuntu1
RUN ln -s /usr/bin/gdb-multiarch /usr/bin/arm-none-eabi-gdb
#cmake
RUN apt -y install cmake-data=3.22.1-1ubuntu1
RUN apt -y install cmake=3.22.1-1ubuntu1
RUN apt -y install git=1:2.34.1-1ubuntu1.4
RUN apt -y install doxygen=1.9.1-2ubuntu2
#Static Analysis
#RUN apt -y install cppcheck=2.7-1
#C++ based Unit tests
#RUN apt -y install cpputest=4.0-2
#Test coverage
#RUN apt -y install lcov=1.15-1