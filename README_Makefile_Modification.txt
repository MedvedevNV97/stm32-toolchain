Building and cleaning unnecessary files.
Insert after make all, before clean section:

clean_build: 
	-make
	-find . -name "*.d" -type f
	-find . -name "*.o" -type f
	-find . -name "*.lst" -type f
	-find . -name "*.d" -type f -delete
	-find . -name "*.o" -type f -delete
	-find . -name "*.lst" -type f -delete

Clean only unnecesary files.
Insert after clean target:

soft_clean:
	-find . -name "*.d" -type f
	-find . -name "*.o" -type f
	-find . -name "*.lst" -type f
	-find . -name "*.d" -type f -delete
	-find . -name "*.o" -type f -delete
	-find . -name "*.lst" -type f -delete

Unnecessary, flashing using OpenOCD.
Insert after OpenOCD:

prog: $(BUILD_DIR)/$(TARGET).elf
    openocd -f interface/stlink.cfg -f target/stm32f4x.cfg -c "program build/$(TARGET).elf verify exit reset"