Для использования GCC ARM+Docker+VSCode:
1) Установить VSCode
2) Установить расширение для VSCode Dev Containers 
(ms-vscode-remote.remote-containers)
3) Добавить в проект файлы:
.devcontainer/devcontainer.json
Dockerfile
4) Запустить Docker Desktop
5) Открыть папку с проектом в VSCode
6) F1 или CTRL+SHIFT+P -> Remote-Containers: Open Folder in Containers... 
VSCode начнет загрузку компонентов и сборку образа
7) Когда контейнер запустится, можно работать, используя терминал 
VSCode. 
8) После закрытия VSCode контейнер не удаляется
P.S. Проект не копируется в контейнер, а подключается папка к нему,
так что изменения в проекте внутри образа останутся, если его закрыть.